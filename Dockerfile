FROM rockylinux:9

# Environment
ENV APP_ROOT /webroot
ENV LOG_DIR $APP_ROOT/log
ENV PS1 "\n\n>> httpd \W \$ "
ENV ONDOCKER 1
WORKDIR $APP_ROOT
ENV PATH $APP_ROOT/bin:$PATH
ENV COMPOSER_HOME=/opt/composer
ENV TERM=xterm

# Package Repositories
RUN dnf install -y \
            dnf-utils \
            https://rpms.remirepo.net/enterprise/remi-release-9.rpm \
            epel-release \
    && dnf -y --setopt=tsflags=nodocs update \
    && dnf -y clean all

ARG PHP_VER=7.4
RUN dnf module reset php \
    && dnf  -y module install php:remi-$PHP_VER

# Packages
ENV PACKAGES \
    php-opcache \
    php-gd \
    php-soap \
    php-fpm \
    php-mysql

RUN dnf -y --setopt=tsflags=nodocs install $PACKAGES \
    && dnf clean all

# Command line utilities
## Composer
RUN curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer


RUN mkdir -p /opt/composer \
    && chown -R 1001:root /opt/composer \
    && chmod -R g=u /opt/composer /etc/passwd
COPY resources/auth.json /opt/composer/auth.json

## Set permissions
RUN chown -R 1001:root -R $APP_ROOT /var/lib/php

## Entrypoint
COPY resources/entrypoint.sh /usr/local/bin/entrypoint.sh

# Server config
RUN rm -rf /etc/php-fpm.d/www.conf
COPY resources/php.ini /etc/php.ini
COPY resources/pool.conf /etc/php-fpm.d/pool.conf
# COPY resources/opcache.ini /etc/php.d/10-opcache.ini
RUN mkdir /run/php-fpm

## Set permissions for application
RUN chown -R 1001:root /var/log /run /usr/local/bin/entrypoint.sh
RUN chmod -R g=u /etc/passwd /var/log /run /usr/local/bin/*
RUN chmod u+x  /usr/local/bin/entrypoint.sh  /usr/local/bin/*


# Ready
STOPSIGNAL SIGQUIT
USER 1001
EXPOSE 9000
CMD ["php-fpm", "-F"]
