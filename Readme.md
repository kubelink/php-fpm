## Apache HTTP server Docker image
### Non-root Docker image running Centos Linux, httpd and PHP 7x

### Base docker image

 - [hub.docker.com](https://hub.docker.com/r/eeacms/apache)

### Source Repository

- [GitLab](https://gitlab.com/kubelink/httpd)

### Deploy via Helm

```console
export HELM_EXPERIMENTAL_OCI=1
helm chart pull registry.gitlab.com/kubelink/httpd:helm-chart
helm chart export registry.gitlab.com/kubelink/httpd:helm-chart .
helm install [releasename] httpd 

hs install apache /progdata/code/httpd/helm-chart \
        --namespace default \
        --set ingress.hostname=ks1.sites.kubelink.com
```


You should see all components installed and httpd pod running
 Then go http://httpd-[your-release-name].sites.kubelink.com  web site you should see "Testing 123.." 
